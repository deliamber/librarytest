﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LibraryUI.Models
{
	public class BookViewModel
	{
		// ID книги
		public int? Id { get; set; }

		// Название книги
		[Required]
		[MaxLength(50)]
		[Display(Name = "Название книги")]
		public string Name { get; set; }

		// Год издания
		[Required]
		[Display(Name = "Год издания")]
		public int? Year { get; set; }

		// Жанр
		[Required]
		[MaxLength(30)]
		[Display(Name = "Жанр")]
		public string Genre { get; set; }

		// Доступное количество
		[Required]
		[Display(Name = "Количество книг")]
		public int NumberOfBooks { get; set; }

		// Описание книги
		[Required]
		[MaxLength(500)]
		[Display(Name = "Краткое содержание")]
		public string Description { get; set; }

		[Required]
		[MaxLength(30)]
		[Display(Name = "Автор")]
		public string Author { get; set; }
	}
}