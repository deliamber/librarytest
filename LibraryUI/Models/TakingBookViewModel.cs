﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LibraryUI.Models
{
	public class TakingBookViewModel
	{
		// ID читателя
		[ForeignKey("User")]
		public int ReaderId { get; set; }

		// ID книги
		[ForeignKey("Book")]
		public int BookId { get; set; }

		// Дата взятия книги
		[Required]
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.mm.yyyy}")]
		[Column(TypeName = "date")]
		[Display(Name = "Дата взятия книги")]
		public DateTime TakeBookDate { get; set; }

		// Дата сдачи книги
		[Required]
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.mm.yyyy}")]
		[Column(TypeName = "date")]
		[Display(Name = "Дата сдачи книги")]
		public DateTime ReturnBookDate { get; set; }

		[Display(Name = "Название книги")]
		public string BookName { get; set; }

		[Required]
		[Display(Name = "Номер читательского билета")]
		public string Login { get; set; }
	}
}