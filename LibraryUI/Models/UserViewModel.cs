﻿using System.ComponentModel.DataAnnotations;

namespace LibraryUI.Models
{
	public class UserViewModel
	{
		// ID пользователя
		public int? Id { get; set; }

		// Роль
		public string Role { get; set; }

		// ФИО
		[Required]
		[Display(Name = "ФИО")]
		[MaxLength(30)]
		public string Name { get; set; }

		[Required]
		[Display(Name = "Логин")]
		[MaxLength(30)]
		// Логин
		public string Login { get; set; }

		[Required]
		[Display(Name = "Пароль")]
		[MaxLength(30)]
		// Пароль
		public string Password { get; set; }

		[Required]
		[Display(Name = "Почтовый адрес")]
		[MaxLength(100)]
		// Почтовый адрес
		public string PostAddress { get; set; }
	}
}