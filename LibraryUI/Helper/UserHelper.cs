﻿
using LibraryUI.LibraryService;
using System.Web;

namespace LibraryUI.Helper
{
	public static class UserHelper
	{
		public static bool IsInRole(string role, HttpRequest request)
		{
			if (request.Cookies["LibraryUser"] != null)
			{
				LibraryServiceClient blLayer = new LibraryServiceClient();
				return blLayer.GetRole(request.Cookies["LibraryUser"].Value) == role;
			}
			return false;
		}
	}
}