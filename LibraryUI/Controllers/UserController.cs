﻿using LibraryBL.Dto;
using LibraryBL.Enums;
using LibraryUI.Helper;
using LibraryUI.LibraryService;
using LibraryUI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Helpers;

namespace LibraryUI.Controllers
{
    public class UserController : Controller
    {
		public ActionResult UserList()
		{
			if (UserHelper.IsInRole(UserRoleList.LIBRARIAN, System.Web.HttpContext.Current.Request) ||
				UserHelper.IsInRole(UserRoleList.ADMIN, System.Web.HttpContext.Current.Request))
			{
				List<UserDto> listOfUsers = new List<UserDto>();
				LibraryServiceClient businessLogicLayer = new LibraryServiceClient ();
				string role = UserRoleList.LIBRARIAN;

				if (UserHelper.IsInRole(UserRoleList.LIBRARIAN, System.Web.HttpContext.Current.Request))
					role = UserRoleList.READER;

				listOfUsers = businessLogicLayer.GetUserListByRole(role);
				ViewBag.Users = listOfUsers;

				return View();
			}
			return RedirectToAction("Login", "Account");
		}

		[HttpPost]
		public ActionResult UserList(UserViewModel userModel)
		{
			if (UserHelper.IsInRole(UserRoleList.LIBRARIAN, System.Web.HttpContext.Current.Request) ||
				UserHelper.IsInRole(UserRoleList.ADMIN, System.Web.HttpContext.Current.Request))
			{
				List<UserDto> listOfUsers = new List<UserDto>();
				LibraryServiceClient businessLogicLayer = new LibraryServiceClient ();
				string role = UserRoleList.LIBRARIAN;

				if (UserHelper.IsInRole(UserRoleList.LIBRARIAN, System.Web.HttpContext.Current.Request))
					role = UserRoleList.READER;

				listOfUsers = businessLogicLayer.GetUserListByParameters(role, userModel.Login, userModel.Name);
				ViewBag.Users = listOfUsers;

				return View();
			}
			return RedirectToAction("Login", "Account");
		}

		[HttpGet]
		public ActionResult EditUser(int Id)
		{
			if (UserHelper.IsInRole(UserRoleList.LIBRARIAN, System.Web.HttpContext.Current.Request) ||
				UserHelper.IsInRole(UserRoleList.ADMIN, System.Web.HttpContext.Current.Request))
			{
				LibraryServiceClient businessLogicLayer = new LibraryServiceClient ();
				ViewBag.user = businessLogicLayer.GetUserById(Id);

				return View();
			}
			return RedirectToAction("Login", "Account");
		}

		[HttpPost]
		public ActionResult EditUser(UserViewModel userModel)
		{
			if (UserHelper.IsInRole(UserRoleList.LIBRARIAN, System.Web.HttpContext.Current.Request) ||
				UserHelper.IsInRole(UserRoleList.ADMIN, System.Web.HttpContext.Current.Request))
			{
				if (ModelState.IsValid)
				{
					LibraryServiceClient businessLogicLayer = new LibraryServiceClient();
					UserDto user = new UserDto();

					user.Login = userModel.Login;
					user.Password = userModel.Password;
					user.Name = userModel.Name;
					user.PostAddress = userModel.PostAddress;
					user.Role = userModel.Role;
					user.Id = userModel.Id;

					businessLogicLayer.EditUser(user);

					if (user.Login == System.Web.HttpContext.Current.Request.Cookies["LibraryUser"].Value)
						return RedirectToAction("Index", "Home");

					return RedirectToAction("UserList", "User");
				}
				return View();
			}
			return RedirectToAction("Login", "Account");
		}

		[HttpGet]
		public ActionResult DeleteUser(int Id)
		{
			if (UserHelper.IsInRole(UserRoleList.LIBRARIAN, System.Web.HttpContext.Current.Request) ||
				UserHelper.IsInRole(UserRoleList.ADMIN, System.Web.HttpContext.Current.Request))
			{
				LibraryServiceClient businessLogicLayer = new LibraryServiceClient ();

				businessLogicLayer.DeleteUser(Id);

				return RedirectToAction("UserList", "User");
			}

			return RedirectToAction("Login", "Account");
		}

		[HttpGet]
		public ActionResult InfoUser(int Id)
		{
			LibraryServiceClient businessLogicLayer = new LibraryServiceClient ();

			if (UserHelper.IsInRole(UserRoleList.ADMIN, System.Web.HttpContext.Current.Request))
			{
				ViewBag.user = businessLogicLayer.GetUserById(Id);
				return View("./InfoLibrarian");
			}
			else if (UserHelper.IsInRole(UserRoleList.LIBRARIAN, System.Web.HttpContext.Current.Request))
			{
				LibraryServiceClient blLayer = new LibraryServiceClient();
				List<TakingBookDto> takingBookList = new List<TakingBookDto>();
				var user = businessLogicLayer.GetUserById(Id);
				takingBookList = blLayer.GetTakingBookListByLogin(user.Login);
				ViewBag.books = takingBookList;
				ViewBag.id = RouteData.Values["id"];
				return View("./InfoReader");
			}
			return RedirectToAction("Login", "Account");
		}

		public ActionResult CreateReaderChart(int readerId)
		{
			LibraryServiceClient blLayer = new LibraryServiceClient();
			List<HistoryLogDto> log = blLayer.GetHistoryLog(readerId);
			List<string> xValue = new List<string>();
			List<string> yValue = new List<string>();
			int bookNumber = 0;

			var HistoryListGroupByDate = log.GroupBy(l => l.EventDate);

			foreach (var logNote in HistoryListGroupByDate)
			{
				string xTmp;
				
				xTmp = logNote.Key.ToString("dd.MM.yyyy");
				xValue.Add(xTmp);

				var logList = logNote.ToList();
				bookNumber += logList.Where(l => l.EventId == LibraryContracts.Enums.EventType.Take).Count();
				bookNumber -= logList.Where(l => l.EventId == LibraryContracts.Enums.EventType.Return).Count();
				yValue.Add(bookNumber.ToString());
			}

			var chart = new Chart(width: 700, height: 300, theme: ChartTheme.Green)
				  .AddTitle("График количества книг по датам")
				  .AddSeries(
						 chartType: "Line",
						 xValue: xValue,
						 yValues: yValue)
				  .Write();

			return null;
		}

		public ActionResult CreateTakeActivityChart()
		{
			LibraryServiceClient blLayer = new LibraryServiceClient();
			List<HistoryLogDto> log = blLayer.GetOverallHistoryLog();
			List<HistoryLogDto> Takelog = log.Where(l => l.EventId == LibraryContracts.Enums.EventType.Take).ToList();
			List<string> xTakeValue = new List<string>();
			List<string> yTakeValue = new List<string>();

			var HistoryTakeListGroupByDate = Takelog.GroupBy(l => l.EventDate);

			foreach (var logNote in HistoryTakeListGroupByDate)
			{
				int bookNumber = 0;
				string xTmp;

				xTmp = logNote.Key.ToString("dd.MM.yyyy");
				xTakeValue.Add(xTmp);

				var logList = logNote.ToList();
				bookNumber += logList.Count();
				yTakeValue.Add(bookNumber.ToString());
			}

			var chart = new Chart(width: 500, height: 300, theme: ChartTheme.Green)
					.AddTitle("График раздачи книг по дням")
					.AddSeries(
						 chartType: "Line",
						 markerStep: 5,
						 xValue: xTakeValue,
						 yValues: yTakeValue)
				  .Write();

			return null;
		}

		public ActionResult CreateReturnActivityChart()
		{
			LibraryServiceClient blLayer = new LibraryServiceClient();
			List<HistoryLogDto> log = blLayer.GetOverallHistoryLog();
			List<HistoryLogDto> Returnlog = log.Where(l => l.EventId == LibraryContracts.Enums.EventType.Return).ToList();
			List<string> xReturnValue = new List<string>();
			List<string> yReturnValue = new List<string>();

			var HistoryReturnListGroupByDate = Returnlog.GroupBy(l => l.EventDate);

			foreach (var logNote in HistoryReturnListGroupByDate)
			{
				int bookNumber = 0;
				string xTmp;

				xTmp = logNote.Key.ToString("dd.MM.yyyy");
				xReturnValue.Add(xTmp);

				var logList = logNote.ToList();
				bookNumber += logList.Count();
				yReturnValue.Add(bookNumber.ToString());
			}

			var chart = new Chart(width: 500, height: 300, theme: ChartTheme.Green)
					.AddTitle("График приема книг по дням")
					.AddSeries(
						 chartType: "Line",
						 markerStep: 5,
						 xValue: xReturnValue,
						 yValues: yReturnValue)
				  .Write();

			return null;
		}
	}
}