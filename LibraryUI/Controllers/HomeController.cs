﻿
using System.Web.Mvc;

namespace LibraryUI.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}
	}
}