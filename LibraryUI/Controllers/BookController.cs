﻿using LibraryBL.Dto;
using LibraryBL.Enums;
using LibraryUI.Helper;
using LibraryUI.LibraryService;
using LibraryUI.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace LibraryUI.Controllers
{
    public class BookController : Controller
    {
		public ActionResult BookList()
		{
			List<BookDto> listOfBooks = new List<BookDto>();
			LibraryServiceClient businessLogicLayer = new LibraryServiceClient();

			listOfBooks = businessLogicLayer.GetBookList();
			ViewBag.Books = listOfBooks;

			return View();
		}

		[HttpPost]
		public ActionResult BookList(BookViewModel bookModel)
		{
			List<BookDto> listOfBooks = new List<BookDto>();
			LibraryServiceClient businessLogicLayer = new LibraryServiceClient ();

			listOfBooks = businessLogicLayer.GetBookListByParameters(bookModel.Author, bookModel.Year);
			ViewBag.Books = listOfBooks;

			return View();
		}

		[HttpGet]
		public ActionResult InfoBook(int Id)
		{
			LibraryServiceClient businessLogicLayer = new LibraryServiceClient ();

			ViewBag.book = businessLogicLayer.GetBookById(Id);

			return View();
		}

		[HttpGet]
		public ActionResult EditBook(int Id)
		{
			if (UserHelper.IsInRole(UserRoleList.LIBRARIAN, System.Web.HttpContext.Current.Request))
			{
				LibraryServiceClient businessLogicLayer = new LibraryServiceClient ();

				ViewBag.book = businessLogicLayer.GetBookById(Id);

				return View();
			}

			return RedirectToAction("Login", "Account");
		}

		[HttpPost]
		public ActionResult EditBook(BookViewModel bookModel)
		{
			if (UserHelper.IsInRole(UserRoleList.LIBRARIAN, System.Web.HttpContext.Current.Request))
			{
				if (ModelState.IsValid)
				{
					LibraryServiceClient businessLogicLayer = new LibraryServiceClient ();
					BookDto book = new BookDto();

					book.Author = bookModel.Author;
					book.Description = bookModel.Description;
					book.Genre = bookModel.Genre;
					book.Name = bookModel.Name;
					book.Year = (int)bookModel.Year;
					book.InfoAboutAvailable = bookModel.NumberOfBooks.ToString();
					book.Id = bookModel.Id;

					businessLogicLayer.EditBook(book);

					return RedirectToAction("BookList", "Book");
				}
				return View();
			}
			return RedirectToAction("Login", "Account");
		}

		[HttpGet]
		public ActionResult DeleteBook(int Id)
		{
			if (UserHelper.IsInRole(UserRoleList.LIBRARIAN, System.Web.HttpContext.Current.Request))
			{
				LibraryServiceClient businessLogicLayer = new LibraryServiceClient();

				businessLogicLayer.DeleteBook(Id);

				return RedirectToAction("BookList", "Book");
			}

			return RedirectToAction("Login", "Account");
		}

		public ActionResult AddBook()
		{
			if (UserHelper.IsInRole(UserRoleList.LIBRARIAN, System.Web.HttpContext.Current.Request))
			{
				return View();
			}

			return RedirectToAction("Login", "Account");
		}

		[HttpPost]
		public ActionResult AddBook(BookViewModel bookModel)
		{
			if (UserHelper.IsInRole(UserRoleList.LIBRARIAN, System.Web.HttpContext.Current.Request))
			{
				if (ModelState.IsValid)
				{
					LibraryServiceClient businessLogicLayer = new LibraryServiceClient();
					BookDto book = new BookDto();

					book.Author = bookModel.Author;
					book.Description = bookModel.Description;
					book.Genre = bookModel.Genre;
					book.Name = bookModel.Name;
					book.Year = (int)bookModel.Year;
					book.InfoAboutAvailable = bookModel.NumberOfBooks.ToString();

					businessLogicLayer.AddBook(book);

					return RedirectToAction("BookList", "Book");
				}
				return View();
			}
			return RedirectToAction("Login", "Account");
		}

		public ActionResult ReturnBook(int Id)
		{
			if (UserHelper.IsInRole(UserRoleList.LIBRARIAN, System.Web.HttpContext.Current.Request))
			{
				LibraryServiceClient blLayer = new LibraryServiceClient();

				blLayer.ReturnBook(Id);

				return RedirectToAction("UserList", "User");

			}
			return RedirectToAction("Login", "Account");
		}

		public ActionResult ProlongateReturnBook(int noteId, int daysCount)
		{
			LibraryServiceClient blLayer = new LibraryServiceClient();

			blLayer.ProlongateReturnBook(noteId, daysCount);

			return Json( new { success = true }, JsonRequestBehavior.AllowGet);
		}

		public ActionResult TakeBook(int Id)
		{
			TakingBookViewModel book = new TakingBookViewModel();
			LibraryServiceClient blLayer = new LibraryServiceClient();
			book.BookName = blLayer.GetBookById(Id).Name;
			book.BookId = Id;
			return View(book);
		}

		[HttpPost]
		public ActionResult TakeBook(TakingBookViewModel note)
		{
			if (ModelState.IsValid)
			{
				LibraryServiceClient businessLogicLayer = new LibraryServiceClient();
				TakingBookDto noteDto = new TakingBookDto();
				UserDto reader = businessLogicLayer.GetUserListByParameters(UserRoleList.READER, note.Login, null)[0];
				if (businessLogicLayer.CanTakeBook((int)reader.Id))
				{
					noteDto.BookId = note.BookId;
					noteDto.ReaderId = (int)businessLogicLayer.GetUserListByParameters(UserRoleList.READER, note.Login, null)[0].Id;
					noteDto.TakeBookDate = note.TakeBookDate;
					noteDto.ReturnBookDate = note.ReturnBookDate;

					businessLogicLayer.TakeBook(noteDto);
				}
				else
				{
					ModelState.AddModelError("", "У читателя на руках 15 книг");
					return View(note);
				}
					
				return RedirectToAction("BookList", "Book");
			}
			return View(note);
		}
	}
}