﻿using LibraryUI.Helper;
using LibraryUI.Models;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using LibraryUI.LibraryService;
using LibraryBL.Enums;
using LibraryBL.Dto;

namespace LibraryUI.Controllers
{
	public class AccountController : Controller
	{
		// GET: Register
		public ActionResult Register()
		{
			if (UserHelper.IsInRole(UserRoleList.LIBRARIAN, System.Web.HttpContext.Current.Request) ||
				UserHelper.IsInRole(UserRoleList.ADMIN, System.Web.HttpContext.Current.Request))
			{
				return View();
			}
			return RedirectToAction("Login", "Account");
		}

		// POST: Register
		[HttpPost]
		public ActionResult Register(UserViewModel userModel)
		{
			if (UserHelper.IsInRole(UserRoleList.LIBRARIAN, System.Web.HttpContext.Current.Request) ||
				UserHelper.IsInRole(UserRoleList.ADMIN, System.Web.HttpContext.Current.Request))
			{
				if (ModelState.IsValid)
				{
					LibraryServiceClient businessLogicLayer = new LibraryServiceClient ();
					UserDto user = new UserDto();
					user.Login = userModel.Login;
					user.Password = userModel.Password;
					user.Name = userModel.Name;
					user.PostAddress = userModel.PostAddress;
					if (UserHelper.IsInRole(UserRoleList.LIBRARIAN, System.Web.HttpContext.Current.Request))
						user.Role = UserRoleList.READER;
					else
						user.Role = UserRoleList.LIBRARIAN;

					businessLogicLayer.AddUser(user);

					return RedirectToAction("UserList", "User");
				}
				return View();
			}
			return RedirectToAction("Login", "Account");
		}

		// GET: Login
		public ActionResult Login()
		{
			return View();
		}

		// POST: Login
		[HttpPost]
		public ActionResult Login(UserViewModel userModel, string returnUrl)
		{
				int? userId = ValidateUser(userModel.Login, userModel.Password);
				if (userId != null)
				{
					var cookie = new HttpCookie("LibraryUser");
					cookie.Value = userModel.Login.ToString();
					cookie.Expires = DateTime.Now.AddHours(2.0);
					Response.SetCookie(cookie);

					if (Url.IsLocalUrl(returnUrl))
					{
						return Redirect(returnUrl);
					}
					else
					{
						return RedirectToAction("Index", "Home");
					}
				}
				else
				{
					ModelState.AddModelError("", "Неправильный пароль или логин");
				}
			
			return View();
		}

		public ActionResult LogOff()
		{
			// проверка на существование cookie
			if (Request.Cookies["LibraryUser"] != null)
			{
				// установка срока
				Response.Cookies["LibraryUser"].Expires = DateTime.Now.AddDays(-1);
			}

			return RedirectToAction("Index", "Home");
		}

		public ActionResult ManageAccount()
		{
			HttpRequest userRequest = System.Web.HttpContext.Current.Request;
			if (UserHelper.IsInRole(UserRoleList.LIBRARIAN, userRequest))
			{
				LibraryServiceClient blLayer = new LibraryServiceClient();
				//LibraryServiceClient blLayer = new LibraryServiceClient ();
				UserDto currentUser = blLayer.GetUserListByParameters(UserRoleList.LIBRARIAN, userRequest.Cookies["LibraryUser"].Value, null)[0];
				ViewBag.user = currentUser;
				return View("./ManageLibrarianAccount");
			}
			if (UserHelper.IsInRole(UserRoleList.READER, userRequest))
			{
				LibraryServiceClient blLayer = new LibraryServiceClient ();
				List<TakingBookDto> takingBookList = new List<TakingBookDto>();
				takingBookList = blLayer.GetTakingBookListByLogin(userRequest.Cookies["LibraryUser"].Value);
				ViewBag.books = takingBookList;
				return View("./ManageReaderAccount");
			}

			return RedirectToAction("Login", "Account");
		}

		private static int? ValidateUser(string login, string password)
		{
			LibraryServiceClient businessLogicLayer = new LibraryServiceClient ();
			return businessLogicLayer.ValidateUser(login, password);
		}
	}
}