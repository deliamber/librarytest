﻿jQuery(document).ready(function ($) {
	$(".clickable-row").click(function () {
		window.location = $(this).data("href");
	});
	$('.datepicker').datepicker({
		startDate: "+1d",
		endDate: "+14d",
		format: "dd.mm.yyyy",
		language: "ru",
	});
});
