﻿
using LibraryBL.Dto;
using LibraryContracts.Enums;
using LibraryDAL;
using LibraryDAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace LibraryWCFService
{
	public class LibraryService : ILibraryService
	{
		#region Работа с книгами
		public void AddBook(BookDto bookDto)
		{
			Book book = new Book();

			book = ConvertBookDtoToBook(bookDto);

			DataAccessLayer dataAccessLayer = new DataAccessLayer();
			dataAccessLayer.AddBook(book);
		}

		public void EditBook(BookDto bookDto)
		{
			Book book = new Book();

			book = ConvertBookDtoToBook(bookDto);

			DataAccessLayer dataAccessLayer = new DataAccessLayer();
			dataAccessLayer.EditBook(book);
		}

		public void DeleteBook(int bookId)
		{
			DataAccessLayer dataAccessLayer = new DataAccessLayer();
			dataAccessLayer.DeleteBook(bookId);
		}

		public BookDto GetBookById(int bookId)
		{
			DataAccessLayer dataAccessLayer = new DataAccessLayer();
			Book book = new Book();
			BookDto bookDto = new BookDto();

			book = dataAccessLayer.GetBookById(bookId);

			bookDto.Author = book.Author;
			bookDto.Name = book.Name;
			bookDto.Id = book.Id;
			bookDto.Description = book.Description;
			bookDto.Genre = book.Genre;
			bookDto.Year = book.Year;
			bookDto.InfoAboutAvailable = book.NumberOfBooks.ToString();

			return bookDto;
		}

		public List<BookDto> GetBookList()
		{
			DataAccessLayer dataAccessLayer = new DataAccessLayer();
			List<Book> bookList = new List<Book>();

			bookList = dataAccessLayer.GetBookList();

			return GenerateBookList(bookList);
		}

		public List<BookDto> GetBookListByParameters(string bookAuthor, int? bookYear)
		{
			DataAccessLayer dataAccessLayer = new DataAccessLayer();
			List<Book> bookList = new List<Book>();

			bookList = dataAccessLayer.GetBookListByParameters(bookAuthor, bookYear);

			return GenerateBookList(bookList);
		}

		public List<TakingBookDto> GetTakingBookListByLogin(string userLogin)
		{
			DataAccessLayer dataAccessLayer = new DataAccessLayer();
			List<TakingBookDto> takingBookDtoList = new List<TakingBookDto>();
			List<TakingBook> takingBookList = new List<TakingBook>();

			takingBookList = dataAccessLayer.GetTakingBookListByLogin(userLogin);

			foreach (TakingBook TakingBook in takingBookList)
			{
				TakingBookDto tmpNote = new TakingBookDto();
				tmpNote.BookId = TakingBook.BookId;
				tmpNote.ReaderId = TakingBook.ReaderId;
				tmpNote.TakeBookDate = TakingBook.TakeBookDate;
				tmpNote.ReturnBookDate = TakingBook.ReturnBookDate;
				tmpNote.BookName = dataAccessLayer.GetBookById(tmpNote.BookId).Name;
				tmpNote.CanProlongate = CanProlongate(TakingBook.ReaderId);
				tmpNote.CanTakeBook = CanTakeBook(TakingBook.ReaderId);
				tmpNote.Id = TakingBook.Id;

				takingBookDtoList.Add(tmpNote);
			}

			return takingBookDtoList;
		}

		public void ProlongateReturnBook(int noteId, int daysCount)
		{
			DataAccessLayer daLayer = new DataAccessLayer();
			daLayer.ProlongateReturnBook(noteId, daysCount);
		}

		public void ReturnBook(int noteId)
		{
			DataAccessLayer daLayer = new DataAccessLayer();
			daLayer.ReturnBook(noteId);
		}

		public void TakeBook(TakingBookDto note)
		{
			TakingBook takingBookNote = new TakingBook();
			takingBookNote.BookId = note.BookId;
			takingBookNote.IsReturned = false;
			takingBookNote.ReaderId = note.ReaderId;
			takingBookNote.ReturnBookDate = note.ReturnBookDate;
			takingBookNote.TakeBookDate = note.TakeBookDate;

			DataAccessLayer daLayer = new DataAccessLayer();
			daLayer.TakeBook(takingBookNote);
		}
		#endregion

		#region Работа с пользователем
		public int AddUser(UserDto userDto)
		{
			User user = new User();
			user = ConvertUserDtoToUser(userDto);

			DataAccessLayer dataAccessLayer = new DataAccessLayer();
			int userId = dataAccessLayer.AddUser(user);
			return userId;
		}

		public int? ValidateUser(string login, string password)
		{
			DataAccessLayer dataAccessLayer = new DataAccessLayer();
			return dataAccessLayer.ValidateUser(login, password);
		}

		public void EditUser(UserDto userDto)
		{
			User user = new User();

			user = ConvertUserDtoToUser(userDto);
			user.Id = (int)userDto.Id;

			DataAccessLayer dataAccessLayer = new DataAccessLayer();
			dataAccessLayer.EditUser(user);
		}

		public void DeleteUser(int userId)
		{
			DataAccessLayer dataAccessLayer = new DataAccessLayer();
			dataAccessLayer.DeleteUser(userId);
		}

		public string GetRole(string login)
		{
			DataAccessLayer dataAccessLayer = new DataAccessLayer();
			return dataAccessLayer.GetRole(login);
		}

		public UserDto GetUserById(int userId)
		{
			DataAccessLayer dataAccessLayer = new DataAccessLayer();
			User user = new User();
			UserDto userDto = new UserDto();

			user = dataAccessLayer.GetUserById(userId);

			userDto.Login = user.Login;
			userDto.Name = user.Name;
			userDto.Id = user.Id;
			userDto.Password = user.Password;
			userDto.PostAddress = user.PostAddress;
			userDto.Role = user.Role;

			return userDto;
		}

		public bool CanProlongate(int userId)
		{
			bool canProlongate = false;
			DataAccessLayer daLayer = new DataAccessLayer();
			InfoAboutReader readerInfo = daLayer.GetReaderInfo(userId);

			if (readerInfo.RemainigBookProlongation != 0)
				canProlongate = true;
			else if (DateTime.Today.Month != readerInfo.LastBookProlongationDate.Month ||
				DateTime.Today.Year != readerInfo.LastBookProlongationDate.Year)
			{
				canProlongate = true;
				daLayer.ResetBookProlongation(userId);
			}

			return canProlongate;
		}

		public bool CanTakeBook(int userId)
		{
			bool canTakeBook = false;
			DataAccessLayer daLayer = new DataAccessLayer();
			InfoAboutReader readerInfo = daLayer.GetReaderInfo(userId);

			if (readerInfo.BookNumber < 15)
				canTakeBook = true;

			return canTakeBook;
		}

		public List<UserDto> GetUserListByRole(string role)
		{
			DataAccessLayer dataAccessLayer = new DataAccessLayer();
			List<User> userList = new List<User>();

			userList = dataAccessLayer.GetUserListByRole(role);

			return GenerateUserList(userList);
		}

		public List<UserDto> GetUserListByParameters(string role, string login, string name)
		{
			DataAccessLayer dataAccessLayer = new DataAccessLayer();
			List<User> userList = new List<User>();

			userList = dataAccessLayer.GetUserListByParameters(role, login, name);

			return GenerateUserList(userList);
		}
		#endregion

		public List<HistoryLogDto> GetHistoryLog(int readerId)
		{
			DataAccessLayer daLayer = new DataAccessLayer();
			List<HistoryLogDto> logDtoList = new List<HistoryLogDto>();
			List<HistoryLog> logList = new List<HistoryLog>();
			logList = daLayer.GetHistoryLog(readerId);
			foreach(HistoryLog logNote in logList)
			{
				HistoryLogDto tmpLog = new HistoryLogDto();
				tmpLog.EventDate = logNote.EventDate;
				tmpLog.EventId = (EventType)logNote.EventId;

				logDtoList.Add(tmpLog);
			}

			return logDtoList;
		}

		public List<HistoryLogDto> GetOverallHistoryLog()
		{
			DataAccessLayer daLayer = new DataAccessLayer();
			List<HistoryLogDto> logDtoList = new List<HistoryLogDto>();
			List<HistoryLog> logList = new List<HistoryLog>();
			logList = daLayer.GetOverallHistoryLog();
			foreach (HistoryLog logNote in logList)
			{
				HistoryLogDto tmpLog = new HistoryLogDto();
				tmpLog.EventDate = logNote.EventDate;
				tmpLog.EventId = (EventType)logNote.EventId;

				logDtoList.Add(tmpLog);
			}

			return logDtoList;
		}

		#region Helpers
		private User ConvertUserDtoToUser(UserDto userDto)
		{
			User user = new User();

			user.Name = userDto.Name;
			user.Login = userDto.Login;
			user.Password = userDto.Password;
			user.PostAddress = userDto.PostAddress;
			user.Role = userDto.Role;

			return user;
		}

		private Book ConvertBookDtoToBook(BookDto bookDto)
		{
			Book book = new Book();

			book.Author = bookDto.Author;
			book.Description = bookDto.Description;
			book.Genre = bookDto.Genre;
			book.Name = bookDto.Name;
			book.Year = bookDto.Year;
			book.NumberOfBooks = System.Convert.ToInt32(bookDto.InfoAboutAvailable);
			if (bookDto.Id.HasValue)
				book.Id = (int)bookDto.Id;

			return book;
		}

		private static List<BookDto> GenerateBookList(List<Book> bookList)
		{
			DataAccessLayer dataAccessLayer = new DataAccessLayer();
			List<BookDto> bookListDto = new List<BookDto>();

			foreach (Book book in bookList)
			{
				BookDto tmpBook = new BookDto();
				tmpBook.Author = book.Author;
				tmpBook.Name = book.Name;
				tmpBook.Id = book.Id;
				tmpBook.NumberOfBooks = book.NumberOfBooks;

				if (book.NumberOfBooks != 0)
				{
					tmpBook.InfoAboutAvailable = (book.NumberOfBooks).ToString() + " шт.";
				}
				else
				{
					string tmpDate = dataAccessLayer.GetReturnBookDate(book.Id);
					tmpBook.InfoAboutAvailable = "Будет в наличие " + tmpDate;
				}

				bookListDto.Add(tmpBook);
			}

			return bookListDto;
		}

		private static List<UserDto> GenerateUserList(List<User> userList)
		{
			DataAccessLayer dataAccessLayer = new DataAccessLayer();
			List<UserDto> userListDto = new List<UserDto>();

			foreach (User user in userList)
			{
				UserDto tmpUser = new UserDto();
				tmpUser.Login = user.Login;
				tmpUser.Id = user.Id;
				tmpUser.Name = user.Name;
				tmpUser.Password = user.Password;
				tmpUser.PostAddress = user.PostAddress;
				tmpUser.Role = user.Role;

				userListDto.Add(tmpUser);
			}

			return userListDto;
		}
		#endregion
	}
}
