﻿using LibraryBL.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace LibraryWCFService
{
	[ServiceContract]
	interface ILibraryService
	{
		/// <summary>
		/// Добавляет книгу в базу
		/// </summary>
		/// <param name="bookDto"></param>
		[OperationContract]
		void AddBook(BookDto bookDto);

		/// <summary>
		/// Редактирует книгу в базе
		/// </summary>
		/// <param name="bookDto"></param>
		[OperationContract]
		void EditBook(BookDto bookDto);

		/// <summary>
		/// Удаляет книгу из всех списков
		/// </summary>
		/// <param name="bookId"></param>
		[OperationContract]
		void DeleteBook(int bookId);

		/// <summary>
		/// Возвращает книгу по Id
		/// </summary>
		/// <param name="bookId"></param>
		/// <returns></returns>
		[OperationContract]
		BookDto GetBookById(int bookId);

		/// <summary>
		/// Возвращает список неудалённых книг
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		List<BookDto> GetBookList();

		/// <summary>
		/// Возвращает список книг в зависимости от параметров поиска
		/// </summary>
		/// <param name="bookAuthor"></param>
		/// <param name="bookYear"></param>
		/// <returns></returns>
		[OperationContract]
		List<BookDto> GetBookListByParameters(string bookAuthor, int? bookYear);

		/// <summary>
		/// Продлевает дату сдачи книги
		/// </summary>
		/// <param name="noteId"></param>
		/// <param name="daysCount"></param>
		[OperationContract]
		void ProlongateReturnBook(int noteId, int daysCount);

		/// <summary>
		/// Возвращение книги пользователем
		/// </summary>
		/// <param name="noteId"></param>
		[OperationContract]
		void ReturnBook(int noteId);

		/// <summary>
		/// Выдача книги читателю
		/// </summary>
		/// <param name="note"></param>
		[OperationContract]
		void TakeBook(TakingBookDto note);

		/// <summary>
		/// Добавляет пользователя в базу
		/// </summary>
		/// <param name="userDto"></param>
		/// <returns></returns>
		[OperationContract]
		int AddUser(UserDto userDto);

		/// <summary>
		/// Проверка пользователя при авторизации
		/// </summary>
		/// <param name="login"></param>
		/// <param name="password"></param>
		/// <returns></returns>
		[OperationContract]
		int? ValidateUser(string login, string password);

		/// <summary>
		/// Редактирование пользователя в базе
		/// </summary>
		/// <param name="userDto"></param>
		[OperationContract]
		void EditUser(UserDto userDto);

		/// <summary>
		/// Удаление пользователя из всех списков
		/// </summary>
		/// <param name="userId"></param>
		[OperationContract]
		void DeleteUser(int userId);

		/// <summary>
		/// Возвращает роль пользователя по логину
		/// </summary>
		/// <param name="login"></param>
		/// <returns></returns>
		[OperationContract]
		string GetRole(string login);

		/// <summary>
		/// Возвращает пользователя по Id
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		[OperationContract]
		UserDto GetUserById(int userId);

		/// <summary>
		/// Может ли пользователь продлить книги
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		[OperationContract]
		bool CanProlongate(int userId);

		/// <summary>
		/// Может ли пользователь брать книгу
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		[OperationContract]
		bool CanTakeBook(int userId);

		/// <summary>
		/// Возвращает список пользователей в зависимости от ролей
		/// </summary>
		/// <param name="role"></param>
		/// <returns></returns>
		[OperationContract]
		List<UserDto> GetUserListByRole(string role);

		/// <summary>
		/// Возвращает список пользователей в зависимости от ролей и параметров поиска
		/// </summary>
		/// <param name="role"></param>
		/// <param name="login"></param>
		/// <param name="name"></param>
		/// <returns></returns>
		[OperationContract]
		List<UserDto> GetUserListByParameters(string role, string login, string name);

		/// <summary>
		/// Возвращает список книг на руках у пользователя
		/// </summary>
		/// <param name="userLogin"></param>
		/// <returns></returns>
		[OperationContract]
		List<TakingBookDto> GetTakingBookListByLogin(string userLogin);

		/// <summary>
		/// Возвращает log активности пользователя
		/// </summary>
		/// <param name="readerId"></param>
		/// <returns></returns>
		[OperationContract]
		List<HistoryLogDto> GetHistoryLog(int readerId);

		/// <summary>
		/// Возвращает log общей активности
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		List<HistoryLogDto> GetOverallHistoryLog();
	}


	

}
