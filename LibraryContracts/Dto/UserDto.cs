﻿using System.Runtime.Serialization;

namespace LibraryBL.Dto
{
	[DataContract]
	public class UserDto
	{
		// Id пользователя
		[DataMember]
		public int? Id { get; set; }
		// ФИО
		[DataMember]
		public string Name { get; set; }
		// Логин
		[DataMember]
		public string Login { get; set; }
		// Пароль
		[DataMember]
		public string Password { get; set; }
		// Почтовый адрес
		[DataMember]
		public string PostAddress { get; set; }
		// Тип пользователя
		[DataMember]
		public string Role { get; set; }
	}
}
