﻿using LibraryContracts.Enums;
using System;
using System.Runtime.Serialization;

namespace LibraryBL.Dto
{
	[DataContract]
	public class HistoryLogDto
	{
		// ID пользователя
		[DataMember]
		public int UserId { get; set; }
		// Дата события 
		[DataMember]
		public DateTime EventDate { get; set; }
		// Тип события: 1 - взял книгу, 2 - отдал книгу
		[DataMember]
		public EventType EventId { get; set; }
	}
}
