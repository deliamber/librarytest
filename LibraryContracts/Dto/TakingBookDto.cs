﻿using System;
using System.Runtime.Serialization;

namespace LibraryBL.Dto
{
	[DataContract]
	public class TakingBookDto
	{
		// ID записи
		[DataMember]
		public int Id { get; set; }
		// ID читателя
		[DataMember]
		public int ReaderId { get; set; }
		// ID книги
		[DataMember]
		public int BookId { get; set; }
		// Дата взятия книги
		[DataMember]
		public DateTime TakeBookDate { get; set; }
		// Дата сдачи книги
		[DataMember]
		public DateTime ReturnBookDate { get; set; }
		// Название книги
		[DataMember]
		public string BookName { get; set; }
		// Может ли читатель продлять книги
		[DataMember]
		public bool CanProlongate { get; set; }
		// Может ли читатель брать книги
		[DataMember]
		public bool CanTakeBook { get; set; }
	}
}
