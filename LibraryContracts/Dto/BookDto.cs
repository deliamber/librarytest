﻿using System.Runtime.Serialization;

namespace LibraryBL.Dto
{
	[DataContract]
	public class BookDto
	{
		// Id книги
		[DataMember]
		public int? Id { get; set; }
		// Название книги
		[DataMember]
		public string Name { get; set; }
		// Год издания
		[DataMember]
		public int Year { get; set; }
		// Жанр
		[DataMember]
		public string Genre { get; set; }
		// Информация о наличие
		[DataMember]
		public string InfoAboutAvailable { get; set; }
		// Доступное количество
		[DataMember]
		public int NumberOfBooks { get; set; }
		// Описание книги
		[DataMember]
		public string Description { get; set; }
		// Автор
		[DataMember]
		public string Author { get; set; }
	}
}
