﻿namespace LibraryContracts.Enums
{
	public enum EventType
	{
		/// <summary>
		/// Взял
		/// </summary>
		Take = 1,
		/// <summary>
		/// Отдал
		/// </summary>
		Return = 2
	}
}
