﻿namespace LibraryBL.Enums
{
	public static class UserRoleList
	{
		// Тип пользователя - Администратор
		public static readonly string ADMIN = "admin";
		// Тип пользователя - Библиотекарь
		public static readonly string LIBRARIAN = "librarian";
		// Тип пользователя - Читатель с читательским билетом
		public static readonly string READER = "reader";
	}
}
