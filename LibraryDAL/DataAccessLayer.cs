﻿using LibraryDAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryDAL
{
	public class DataAccessLayer
	{
		#region Работа с пользователем
		public static readonly int DEFAULT_NUMBER_BOOK_PROLONGATION = 3;

		public int AddUser(User user)
		{
			LibDBContext dbContext = new LibDBContext();
			dbContext.Users.Add(user);

			// Если добавляется читатель - заполнить дополнительные поля
			if (user.Role == "reader")
			{
				InfoAboutReader defaultInfo = new InfoAboutReader(user.Id);
				dbContext.InfoAboutReaders.Add(defaultInfo);
			}

			dbContext.SaveChanges();

			return user.Id;
		}

		public int? ValidateUser (string login, string password)
		{
			int? userId = null;
			LibDBContext libContext = new LibDBContext();

			//Проверка - зарегистрирован ли пользователь
			User user = libContext.Users.SingleOrDefault(User => User.Login == login && User.Password == password);

			if (user != null)
				userId = user.Id;

			return userId;
		}

		public void EditUser(User updateUser)
		{
			LibDBContext dbContext = new LibDBContext();
			User user = dbContext.Users.FirstOrDefault(User => User.Id == updateUser.Id);
			dbContext.Entry(user).CurrentValues.SetValues(updateUser);
			dbContext.SaveChanges();
		}

		public void DeleteUser(int userId)
		{
			LibDBContext dbContext = new LibDBContext();
			User user = dbContext.Users.FirstOrDefault(User => User.Id == userId);
			dbContext.Entry(user).CurrentValues.SetValues(user.IsDelete = true);
			dbContext.SaveChanges();
		}

		public User GetUserById(int userId)
		{
			LibDBContext libContext = new LibDBContext();
			return libContext.Users.First(User => User.Id == userId);
		}

		public InfoAboutReader GetReaderInfo(int userId)
		{
			LibDBContext libContext = new LibDBContext();
			InfoAboutReader returnInfo = libContext.InfoAboutReaders.FirstOrDefault(InfoAboutReader => InfoAboutReader.UserId == userId);
			return returnInfo;
		}

		public void ResetBookProlongation(int userId)
		{
			LibDBContext libContext = new LibDBContext();

			InfoAboutReader reader = libContext.InfoAboutReaders.First(User => User.UserId == userId);
			libContext.Entry(reader).CurrentValues.SetValues(reader.RemainigBookProlongation = DEFAULT_NUMBER_BOOK_PROLONGATION);
			libContext.SaveChanges();
		}

		public List<User> GetUserListByRole(string role)
		{
			LibDBContext libContext = new LibDBContext();

			IQueryable<User> userList = libContext.Users.Where(User => User.IsDelete == false);
			userList = userList.Where(User => User.Role == role);

			return userList.ToList();
		}

		public List<User> GetUserListByParameters(string role, string login, string name)
		{
			LibDBContext libContext = new LibDBContext();

			IQueryable<User> userList = libContext.Users.Where(User => User.IsDelete == false);
			userList = userList.Where(User => User.Role == role);

			if (name != null)
				userList = userList.Where(Book => Book.Name.Contains(name));
			if (login != null)
				userList = userList.Where(Book => Book.Login == login);

			return userList.ToList();
		}

		public string GetRole(string login)
		{
			LibDBContext libContext = new LibDBContext();
			return libContext.Users.SingleOrDefault(User => User.Login == login).Role;
		}

		public List<HistoryLog> GetHistoryLog(int readerId)
		{
			LibDBContext libContext = new LibDBContext();
			return libContext.HistoryLogs.Where(Log => Log.UserId == readerId).ToList();
		}

		public List<HistoryLog> GetOverallHistoryLog()
		{
			LibDBContext libContext = new LibDBContext();
			return libContext.HistoryLogs.Where(Log => Log.Id > 0).ToList();
		}
		#endregion

		#region Работа с книгами
		public void AddBook(Book book)
		{
			LibDBContext dbContext = new LibDBContext();
			dbContext.Books.Add(book);
			dbContext.SaveChanges();
		}

		public void EditBook(Book updateBook)
		{
			LibDBContext dbContext = new LibDBContext();
			Book book = dbContext.Books.FirstOrDefault(Book => Book.Id == updateBook.Id);
			dbContext.Entry(book).CurrentValues.SetValues(updateBook);
			dbContext.SaveChanges();
		}

		public void DeleteBook(int bookId)
		{
			LibDBContext dbContext = new LibDBContext();
			Book book = dbContext.Books.FirstOrDefault(Book => Book.Id == bookId);
			dbContext.Entry(book).CurrentValues.SetValues(book.IsDelete = true);
			dbContext.SaveChanges();
		}

		public Book GetBookById(int bookId)
		{
			LibDBContext libContext = new LibDBContext();
			return libContext.Books.First(Book => Book.Id == bookId);
		}

		public List<Book> GetBookList()
		{
			List<Book> bookList = new List<Book>();
			LibDBContext libContext = new LibDBContext();

			bookList = libContext.Books.Where(Book => Book.IsDelete == false).ToList();

			return bookList;
		}

		public List<Book> GetBookListByParameters(string bookAuthor, int? bookYear)
		{
			LibDBContext libContext = new LibDBContext();

			IQueryable<Book> bookList = libContext.Books.Where(Book => Book.IsDelete == false);

			if (bookAuthor != null)
				bookList = bookList.Where(Book => Book.Author.Contains(bookAuthor));
			if (bookYear != null)
				bookList = bookList.Where(Book => Book.Year == bookYear);

			return bookList.ToList();
		}

		public List<TakingBook> GetTakingBookListByLogin(string userLogin)
		{
			LibDBContext libContext = new LibDBContext();
			int userId = libContext.Users.Single(User => User.Login == userLogin).Id;
			IQueryable<TakingBook> takingBookList = libContext.TakingBooks.Where(TakingBook => TakingBook.IsReturned == false);

			takingBookList = takingBookList.Where(TakingBook => TakingBook.ReaderId == userId);

			return takingBookList.ToList();
		}

		public string GetReturnBookDate (int bookId)
		{
			LibDBContext libContext = new LibDBContext();

			IQueryable<TakingBook> requestBook = libContext.TakingBooks.Where(TakingBook => TakingBook.IsReturned == false);
			requestBook = requestBook.Where(TakingBook => TakingBook.BookId == bookId);
			
			DateTime returnDate = requestBook.Min(TakingBook => TakingBook.ReturnBookDate);

			return returnDate.ToString("dd.MM.yyyy");
		}

		public void ProlongateReturnBook(int noteId, int daysCount)
		{
			LibDBContext libContext = new LibDBContext();
			TakingBook takingBookNote = new TakingBook();
			InfoAboutReader readerInfo = new InfoAboutReader();

			takingBookNote = libContext.TakingBooks.FirstOrDefault(Note => Note.Id == noteId);
			libContext.Entry(takingBookNote).CurrentValues.SetValues(takingBookNote.ReturnBookDate = takingBookNote.ReturnBookDate.AddDays(daysCount));
			readerInfo = libContext.InfoAboutReaders.FirstOrDefault(Note => Note.UserId == takingBookNote.ReaderId);
			libContext.Entry(readerInfo).CurrentValues.SetValues(readerInfo.RemainigBookProlongation--);
			libContext.Entry(readerInfo).CurrentValues.SetValues(readerInfo.LastBookProlongationDate = DateTime.Today);
			libContext.SaveChanges();
		}

		public void ReturnBook(int noteId)
		{
			LibDBContext libContext = new LibDBContext();
			TakingBook takingBookNote = new TakingBook();
			InfoAboutReader readerInfo = new InfoAboutReader();
			Book book = new Book();
			HistoryLog log = new HistoryLog();

			// Помечаем, что книгу вернули
			takingBookNote = libContext.TakingBooks.FirstOrDefault(Note => Note.Id == noteId);
			libContext.Entry(takingBookNote).CurrentValues.SetValues(takingBookNote.IsReturned = true);
			// Уменьшаем количество книг у пользователя
			readerInfo = libContext.InfoAboutReaders.FirstOrDefault(Note => Note.UserId == takingBookNote.ReaderId);
			libContext.Entry(readerInfo).CurrentValues.SetValues(readerInfo.BookNumber--);
			// Увеличиваем количество экземпляров книги
			book = libContext.Books.FirstOrDefault(Note => Note.Id == takingBookNote.BookId);
			libContext.Entry(book).CurrentValues.SetValues(book.NumberOfBooks++);
			// Записываем событие в log
			log.EventDate = DateTime.Today;
			log.EventId = 2;
			log.UserId = takingBookNote.ReaderId;
			libContext.HistoryLogs.Add(log);

			libContext.SaveChanges();
		}

		public void TakeBook(TakingBook note)
		{
			InfoAboutReader readerInfo = new InfoAboutReader();
			Book book = new Book();
			HistoryLog log = new HistoryLog();
			LibDBContext libContext = new LibDBContext();

			libContext.TakingBooks.Add(note);

			// Увеличиваем количество книг у пользователя
			readerInfo = libContext.InfoAboutReaders.FirstOrDefault(Note => Note.UserId == note.ReaderId);
			libContext.Entry(readerInfo).CurrentValues.SetValues(readerInfo.BookNumber++);
			// Увеличиваем количество экземпляров книги
			book = libContext.Books.FirstOrDefault(Note => Note.Id == note.BookId);
			libContext.Entry(book).CurrentValues.SetValues(book.NumberOfBooks--);
			// Записываем событие в log
			log.EventDate = DateTime.Today;
			log.EventId = 1;
			log.UserId = note.ReaderId;
			libContext.HistoryLogs.Add(log);

			libContext.SaveChanges();
		}
		#endregion
	}
}
