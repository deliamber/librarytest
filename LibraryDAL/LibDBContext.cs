﻿using LibraryDAL.Entity;
using System.Data.Entity;

namespace LibraryDAL
{
	public class LibDBContext : DbContext 
	{
		public LibDBContext () : base ("LibDBContext")
		{
		}

		public DbSet <Book> Books { get; set; }
		public DbSet <HistoryLog> HistoryLogs { get; set; }
		public DbSet <InfoAboutReader> InfoAboutReaders { get; set; }
		public DbSet <TakingBook> TakingBooks { get; set; }
		public DbSet <User> Users { get; set; }
	}
}
