﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LibraryDAL.Entity
{
	public class HistoryLog : EntityBase
	{
		// ID пользователя
		[Required]
		[ForeignKey("User")]
		public int UserId { get; set; }

		// Дата события 
		[Required]
		[Column(TypeName = "date")]
		public DateTime EventDate { get; set; }

		// Тип события: 1 - взял книгу, 2 - отдал книгу
		[Required]
		public int EventId { get; set; }

		// Поле для внешнего ключа
		public User User { get; set; } 
	}
}
