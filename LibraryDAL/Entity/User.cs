﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LibraryDAL.Entity
{
    public class User : EntityBase
	{
		public User()
		{
			IsDelete = false;
		}

		// ФИО
		[Required]
		[MaxLength(30)]
        public string Name { get; set; }

        // Логин
		[Required]
		[MaxLength(30)]
		[Index(IsUnique = true)]
		public string Login { get; set; }

		// Пароль
		[Required]
		[MaxLength(30)]
		public string Password { get; set; }

		// Почтовый адрес
		[MaxLength(100)]
		public string PostAddress { get; set; }

		// Тип пользователя
		[Required]
		[MaxLength(30)]
		public string Role { get; set; }

		// Удалён пользователь или нет
		[Required]
		public bool IsDelete { get; set; }
	}
}