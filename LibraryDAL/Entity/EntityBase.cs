﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LibraryDAL.Entity
{
    public class EntityBase
    { 
		// Первичный ключ для всех таблиц
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

		// Метка изменения записи
		[Timestamp]
		public byte[] TS { get; set; }
    }
}
