﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LibraryDAL.Entity
{
    public class TakingBook : EntityBase
	{
        // ID читателя
		[Required]
		[ForeignKey("User")]
        public int ReaderId { get; set; }

		// ID книги
		[Required]
		[ForeignKey("Book")]
		public int BookId { get; set; }

		// Дата взятия книги
		[Required]
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/mm/yyyy}")]
		[Column(TypeName = "date")]
		public DateTime TakeBookDate { get; set; }

		// Дата сдачи книги
		[Required]
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/mm/yyyy}")]
		[Column(TypeName = "date")]
		public DateTime ReturnBookDate { get; set; }

		// Возвращена ли книга
		[Required]
		public bool IsReturned { get; set; } 

		// Поле для внешнего ключа
		public User User { get; set; }

		// Поле для внешнего ключа
		public Book Book { get; set; }
    }
}