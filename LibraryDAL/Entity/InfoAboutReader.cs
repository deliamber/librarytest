﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LibraryDAL.Entity
{
    public class InfoAboutReader : EntityBase
	{
		public InfoAboutReader(int userId)
		{
			UserId = userId;
			BookNumber = 0;
			RemainigBookProlongation = 3;
		}

		public InfoAboutReader()
		{
		}

		// Id пользователя
		[Required]
		[ForeignKey("User")]
		public int UserId { get; set; }

        // Количество книг на руках
		[Required]
        public int BookNumber { get; set; }

		// Количество оставшихся продлений в этом месяце
		[Required]
		public int RemainigBookProlongation { get; set; }

		// Дата последнего продления
		[Required]
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/mm/yyyy}")]
		[Column(TypeName = "date")]
		public DateTime LastBookProlongationDate { get; set; }

		// Поле для внешнего ключа
		public User User { get; set; }
	}
}