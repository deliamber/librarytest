﻿using System.ComponentModel.DataAnnotations;

namespace LibraryDAL.Entity
{
    public class Book : EntityBase
	{
		public Book()
		{
			IsDelete = false;
		}

		// Название книги
		[Required]
		[MaxLength(50)]
		public string Name { get; set; }

		// Год издания
		[Required]
		public int Year { get; set; }

		// Жанр
		[Required]
		[MaxLength(30)]
		public string Genre { get; set; }

		// Доступное количество
		[Required]
		public int NumberOfBooks { get; set; }

		// Описание книги
		[Required]
		[MaxLength(500)]
		public string Description { get; set; }

		// Автор книги
		[Required]
		[MaxLength(30)]
		public string Author { get; set; }

		// Удалена книга или нет
		[Required]
		public bool IsDelete { get; set; }
    }
}